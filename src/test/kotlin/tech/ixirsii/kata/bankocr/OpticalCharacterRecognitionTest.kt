package tech.ixirsii.kata.bankocr

import arrow.core.Either
import arrow.core.Either.Right
import arrow.core.right
import com.google.common.io.Resources
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import tech.ixirsii.kata.error.Error
import java.io.File
import java.net.URI

class OpticalCharacterRecognitionTest {
    @Test
    internal fun `GIVEN account ID WHEN ocr THEN returns account ID`() {
        // Given
        val input: List<String> = listOf(
            "    _  _     _  _  _  _  _  _ ",
            "  | _| _||_||_ |_   ||_||_|| |",
            "  ||_  _|  | _||_|  ||_| _||_|"
        )

        val expected: Either<Error, String> = Right("1234567890")

        // When
        val actual: Either<Error, String> = ocr(input)

        // Then
        assertEquals(expected, actual, "Parsed value should equal expected")
    }

    @Disabled
    @Test
    internal fun `GIVEN test data WHEN ocr THEN returns ID`() {
        // Given
        val expected: Array<Either<Error, String>> = arrayOf(
            "000000000".right(),
            "111111111".right(),
            "222222222".right(),
            "333333333".right(),
            "444444444".right(),
            "555555555".right(),
            "666666666".right(),
            "777777777".right(),
            "888888888".right(),
            "999999999".right(),
            "123456789".right(),
            "000000051".right(),
//            "49006771? ILL".right(),
//            "1234?678? ILL".right(),
//            "711111111".right(),
//            "777777177".right(),
//            "200800000".right(),
//            "333393333".right(),
//            "888888888 AMB".right(),
//            "555555555 AMB".right(),
//            "666666666 AMB".right(),
//            "999999999 AMB".right(),
//            "490067715 AMB".right(),
//            "123456789".right(),
//            "000000051".right(),
//            "490867715".right()
        )

        val inputs: MutableList<MutableList<String>> = ArrayList()
        val testFile: String = getResource("Account IDs.txt")
        val lines: List<String> = testFile.split('\n')

        var lineCount = 1
        for (line: String in lines) {
            if (lineCount % 4 == 0) {
                continue
            }

            val index = lineCount / 4

            if (inputs[index] == null) {
                inputs[index] = ArrayList()
            }

            inputs[index].add(line)

            ++lineCount
        }

        // When
        val actual: List<Either<Error, String>> = inputs.map(::ocr)

        // Then
        assertEquals(expected, actual, "OCR result should equal expected")
    }

    /* **************************************** Private utility methods ***************************************** */

    private fun getResource(uri: String): String = String(Resources.toByteArray(Resources.getResource(uri)))
}
