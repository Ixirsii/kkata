package tech.ixirsii.kata.chop

import arrow.core.Option
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class BinarySearch01Test {
    @Test
    internal fun `GIVEN empty array WHEN binarySearch01 THEN returns empty`() {
        // Given
        val array: Array<Int> = emptyArray()
        val value = 3

        // When
        val actual: Option<Int> = binarySearch01(value, array)

        // Then
        assertTrue(actual.isEmpty(), "Option should be empty")
    }

    @Test
    internal fun `GIVEN array size 1 WHEN binarySearch01 THEN returns index`() {
        // Given
        val array: Array<Int> = arrayOf(1)
        val value01 = 3
        val value02 = 1

        // When
        val actual01: Option<Int> = binarySearch01(value01, array)
        val actual02: Option<Int> = binarySearch01(value02, array)

        // Then
        assertTrue(actual01.isEmpty(), "First result should be empty")
        assertFalse(actual02.isEmpty(), "Second result should not be empty")
        assertEquals(0, actual02.orNull(), "Second result should equal 0")
    }

    @Test
    internal fun `GIVEN array size 3 WHEN binarySearch01 THEN returns index`() {
        // Given
        val array: Array<Int> = arrayOf(1, 3, 5)
        val value01 = 1
        val value02 = 3
        val value03 = 5
        val value04 = 0
        val value05 = 2
        val value06 = 4
        val value07 = 6

        // When
        val actual01: Option<Int> = binarySearch01(value01, array)
        val actual02: Option<Int> = binarySearch01(value02, array)
        val actual03: Option<Int> = binarySearch01(value03, array)
        val actual04: Option<Int> = binarySearch01(value04, array)
        val actual05: Option<Int> = binarySearch01(value05, array)
        val actual06: Option<Int> = binarySearch01(value06, array)
        val actual07: Option<Int> = binarySearch01(value07, array)

        // Then
        assertFalse(actual01.isEmpty(), "First result should not be empty")
        assertEquals(0, actual01.orNull(), "First result should equal 0")
        assertFalse(actual02.isEmpty(), "Second result should not be empty")
        assertEquals(1, actual02.orNull(), "Second result should equal 1")
        assertFalse(actual03.isEmpty(), "Third result should not be empty")
        assertEquals(2, actual03.orNull(), "Third result should equal 2")

        assertTrue(actual04.isEmpty(), "Fourth result should be empty")
        assertTrue(actual05.isEmpty(), "Fifth result should be empty")
        assertTrue(actual06.isEmpty(), "Sixth result should be empty")
        assertTrue(actual07.isEmpty(), "Seventh result should be empty")
    }

    @Test
    internal fun `GIVEN array size 4 WHEN binarySearch01 THEN returns index`() {
        // Given
        val array: Array<Int> = arrayOf(1, 3, 5, 7)
        val value01 = 1
        val value02 = 3
        val value03 = 5
        val value04 = 7
        val value05 = 0
        val value06 = 2
        val value07 = 4
        val value08 = 6
        val value09 = 8

        // When
        val actual01: Option<Int> = binarySearch01(value01, array)
        val actual02: Option<Int> = binarySearch01(value02, array)
        val actual03: Option<Int> = binarySearch01(value03, array)
        val actual04: Option<Int> = binarySearch01(value04, array)
        val actual05: Option<Int> = binarySearch01(value05, array)
        val actual06: Option<Int> = binarySearch01(value06, array)
        val actual07: Option<Int> = binarySearch01(value07, array)
        val actual08: Option<Int> = binarySearch01(value08, array)
        val actual09: Option<Int> = binarySearch01(value09, array)

        // Then
        assertFalse(actual01.isEmpty(), "First result should not be empty")
        assertEquals(0, actual01.orNull(), "First result should equal 0")
        assertFalse(actual02.isEmpty(), "Second result should not be empty")
        assertEquals(1, actual02.orNull(), "Second result should equal 1")
        assertFalse(actual03.isEmpty(), "Third result should not be empty")
        assertEquals(2, actual03.orNull(), "Third result should equal 2")
        assertFalse(actual04.isEmpty(), "Fourth result should not be empty")
        assertEquals(3, actual04.orNull(), "Fourth result should equal 3")

        assertTrue(actual05.isEmpty(), "Fifth result should be empty")
        assertTrue(actual06.isEmpty(), "Sixth result should be empty")
        assertTrue(actual07.isEmpty(), "Seventh result should be empty")
        assertTrue(actual08.isEmpty(), "Eighth result should be empty")
        assertTrue(actual09.isEmpty(), "Ninth result should be empty")
    }
}
