package tech.ixirsii.kata.error

sealed interface Error {
    /** User friendly error message. */
    val message: String

    data class InvalidArgument(
        /** User friendly error message. */
        override val message: String
    ): Error

    data class InvalidCharacter(
        /** User friendly error message. */
        override val message: String
    ): Error
}
