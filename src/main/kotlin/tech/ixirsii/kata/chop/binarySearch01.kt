package tech.ixirsii.kata.chop

import arrow.core.None
import arrow.core.Option
import arrow.core.Some

fun <E: Comparable<E>> binarySearch01(value: E, sortedList: Array<E>): Option<Int> {
    return binarySearch01(0, sortedList.size - 1, value, sortedList)
}

private fun <E: Comparable<E>> binarySearch01(lowIndex: Int, highIndex: Int, value: E, sortedList: Array<E>): Option<Int> {
    return if (sortedList.isEmpty() || value < sortedList[lowIndex] || value > sortedList[highIndex]) {
        None
    } else if (lowIndex == highIndex) {
        if (value == sortedList[lowIndex]) Some(lowIndex) else None
    } else {
        val midpoint = lowIndex + ((highIndex - lowIndex) / 2)
        val midValue = sortedList[midpoint]

        if (value == sortedList[lowIndex]) {
            Some(lowIndex)
        } else if (value == midValue) {
            Some(midpoint)
        } else if (value == sortedList[highIndex]) {
            Some(highIndex)
        } else if (midpoint == lowIndex || midpoint == highIndex) {
            None
        } else if (value < midValue) {
            binarySearch01(lowIndex, midpoint, value, sortedList)
        } else {
            binarySearch01(midpoint, highIndex, value, sortedList)
        }
    }
}
