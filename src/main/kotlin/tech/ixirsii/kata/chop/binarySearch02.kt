package tech.ixirsii.kata.chop

import arrow.core.None
import arrow.core.Option
import arrow.core.Some

fun <E: Comparable<E>> binarySearch02(value: E, array: Array<E>): Option<Int> {
    if (array.isEmpty()) {
        return None
    } else if (array.size == 1) {
        return if (value == array[0]) Some(0) else None
    }

    var lowPoint: Int = 0
    var midpoint: Int  = array.size / 2
    var highPoint: Int = array.size - 1


    do {
        val midValue: E = array[midpoint]

        if (value == array[lowPoint]) {
            return Some(lowPoint)
        } else if (value == midValue) {
            return Some(midpoint)
        } else if (value == array[highPoint]) {
            return Some(highPoint)
        } else if (value < midValue) {
            highPoint = midpoint
            midpoint = lowPoint + (highPoint - lowPoint) / 2
        } else {
            lowPoint = midpoint
            midpoint = lowPoint + (highPoint - lowPoint) / 2
        }
    } while (midpoint in (lowPoint + 1)..<highPoint)
    return None
}
