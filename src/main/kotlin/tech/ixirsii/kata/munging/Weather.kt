package tech.ixirsii.kata.munging

data class Weather(
    val day: Int,
    val maxTemperature,
    val minTemperature,
    val avgTemperature,

)
