package tech.ixirsii.kata.bankocr

import arrow.core.Either
import arrow.core.computations.either
import arrow.core.left
import arrow.core.right
import tech.ixirsii.kata.error.Error

/** How wide a single OCR character is. */
private const val CHARACTER_WIDTH: Int = 3

private const val ZERO: String = """
 _ 
| |
|_|"""

private const val ONE: String = """
   
  |
  |"""

private const val TWO: String = """
 _ 
 _|
|_ """

private const val THREE: String = """
 _ 
 _|
 _|"""

private const val FOUR: String = """
   
|_|
  |"""

private const val FIVE: String = """
 _ 
|_ 
 _|"""

private const val SIX: String = """
 _ 
|_ 
|_|"""

private const val SEVEN: String = """
 _ 
  |
  |"""

private const val EIGHT: String = """
 _ 
|_|
|_|"""

private const val NINE: String = """
 _ 
|_|
 _|"""

/**
 * Map of OCR characters to the
 */
private val OCR_MAP: Map<String, String> = mapOf(
    ZERO to "0",
    ONE to "1",
    TWO to "2",
    THREE to "3",
    FOUR to "4",
    FIVE to "5",
    SIX to "6",
    SEVEN to "7",
    EIGHT to "8",
    NINE to "9"
)

/**
 * Match a list of input lines to digits.
 *
 * @param input Input lines.
 * @return String of matched digits if input is valid, otherwise [Error].
 */
fun ocr(input: List<String>): Either<Error, String> = either.eager {
    validateInput(input).bind()

    val chunks: List<List<String>> = input.map { it.chunked(CHARACTER_WIDTH) }
    val characterDigits: List<String> = reassembleChunks(chunks)
    val digits: List<String> = ocrDigit(characterDigits).bind()

    digits.joinToString("")
}

/* ****************************************************************************************************************** *
 *                                             Private utility functions                                              *
 * ****************************************************************************************************************** */

/**
 * Match a character string to a digit.
 *
 * @param digits
 */
private fun ocrDigit(digits: List<String>): Either<Error.InvalidCharacter, List<String>> {
    return Either.catch {
        digits.map {
            OCR_MAP[it]!!
        }
    }
        .mapLeft { Error.InvalidCharacter("Failed to parse $digits -- ${it.message}") }
}

/**
 * Assemble character strings from list of chunks.
 *
 * @param chunks List of lists of Strings where each string is one line of one character.
 * @return List of character strings.
 */
private fun reassembleChunks(chunks: List<List<String>>): List<String> {
    val digits: MutableList<String> = mutableListOf()

    for (i in (0 until chunks[0].size)) {
        var digit = ""

        for (chunkList in chunks) {
            digit = "$digit\n${chunkList[i]}"
        }

        digits.add(digit)
    }

    return digits
}

/**
 * Validate that the input list is 3 lines long.
 *
 * @param lines List of input lines.
 * @return [Unit] if input is valid, otherwise [Error.InvalidArgument].
 */
private fun validateInput(lines: List<String>): Either<Error.InvalidArgument, Unit> {
    if (lines.size != 3) {
        return Error.InvalidArgument("Input string is not 3 lines long").left()
    }

    return Unit.right()
}
